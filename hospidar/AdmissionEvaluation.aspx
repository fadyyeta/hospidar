﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="AdmissionEvaluation.aspx.cs" Inherits="hospidar.AdmissionEvaluation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Hospice Admission Evaluation
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <link href="Styles/deathInvoice.css" rel="stylesheet" />

    <link href="assets/summernote/summernote.css" rel="stylesheet">
    <style>
        .reportCont p {
            direction: ltr;
        }

        h2.left {
            direction: ltr;
        }
    </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <!-- #Start# Center In Page -->
    <div class="row" style="margin-top: 30px;" dir="rtl">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="card-head">
                    <header>Hospice Admission Evaluation </header>
                </div>
                <div class="card-body ">
                    <div class="col-lg-12 p-t-10">
                        <h4><strong>Hospice Admission Evaluation</strong></h4>
                        <p>click the below button to add data for Hospice Admission Evaluation</p>
                        <button type="button"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-circle btn-primary"
                            data-toggle="modal" data-target="#exampleModalLong">
                            Add</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row" style="margin-top: 30px;" dir="rtl">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="card-head">
                    <header>Report view</header>
                </div>
                <div class="card-body " id="reportCont">
                    <div class="col-lg-12 p-t-10 reportCont">
                        <img class="img-responsive left marginedLeft" style="height: 200px; display: -webkit-inline-box;" src="logo/englishLogo.png" />
                        <h2 class="text-center"><strong>Hospice Evaluation on Admission </strong><span class="dateAdmission tagReplace">day / month / year  </span></h2>
                        <br />
                        <br />
                        <p>
                            Patient Name: <span class="fullName tagReplace">Full Name  </span>Age: <span class="age tagReplace">age  </span>Date: <span class="dateAdmission tagReplace">day / month / year  </span>Date of Admission to our Hospice: <span class="dateAdmission tagReplace">day / month / year  </span>
                            <br />
                        </p>
                        <span class="texty marginedLeft">Main Problem of Patient  </span>
                        <div class="mainProb textyContent marginedLeft tagReplace">
                            1-................................ Duration:.................. 
                                <br />
                            2-................................ Duration:..................
                                <br />
                            3-................................ Duration:.................. 
                                <br />
                        </div>
                        <br />
                        <p>
                            <img class="img img-responsive" src="img/RecentLaboratory.JPG" />
                        </p>
                        <br />
                        <span class="texty marginedLeft">Recent laboratory parameters:   </span>
                        <div class="basicOther textyContent marginedLeft tagReplace">1-................................ Duration:.................. 
                                <br />
                            2-................................ Duration:..................
                                <br />
                            3-................................ Duration:.................. 
                                <br />
                        </div>
                        <br />

                        <hr />
                        <h2 class="text-left left marginedLeft">1. Vital signs: </h2>
                        <p>
                            Pulse: <span class="vitalPulse tagReplace">vitalPulse  </span>/ min. - BP: <span class="vitalBP tagReplace">vitalBP  </span>-Temperature: <span class="vitaltemprature tagReplace">vitaltemprature  </span>C - RR: <span class="vitalRR tagReplace">vitalRR  </span>/ min. O2 Saturation: <span class="vitalO2Saturation tagReplace">vitalO2Saturation </span>% On Room air / O2 mask.  
                           <br />

                        </p>
                        <hr />
                        <h2 class="text-left left marginedLeft">2. Neurological State:  </h2>
                        <p>
                            -Glasgow coma Total score: <span class="NeurologicalGlasgow tagReplace">NeurologicalGlasgow  </span>Eye: <span class="NeurologicalEye tagReplace">NeurologicalEye  </span>/4 Verbal: <span class="NeurologicalVerbal tagReplace">NeurologicalVerbal  </span>/5 Movement<span class="NeurologicalMovement tagReplace">NeurologicalMovement  </span>/6
                            <br />
                            -Any residual deficit: <span class="NeurologicalResidualDeficit tagReplace">NeurologicalResidualDeficit </span>
                            <br />
                            CT or MRI of Brain summary:  <span class="NeurologicalMRI tagReplace">NeurologicalMRI </span>
                            <br />

                        </p>
                        <hr />
                        <h2 class="text-left left marginedLeft">3- Chest Condition:   </h2>
                        <p>
                            -Air entry: <span class="chestAirEntry tagReplace">chestAirEntry  </span>Crepitations: <span class="chestCrepitations tagReplace">chestCrepitations  </span>Wheezes: <span class="chestWheezes tagReplace">chestWheezes  </span>
                            <br />
                            -X ray chest:<span class="chestXRay tagReplace">chestXRay   </span>
                            <br />
                            -Oxygen saturation on room air: <span class="chestSatO2 tagReplace">chestSatO2 </span>%. Need for supplemental O2<span class="chestSupO2 tagReplace">chestSupO2 </span>
                            <br />
                            <br />

                        </p>
                        <hr />
                        <h2 class="text-left left marginedLeft">4-Musculoskeletal Condition:  </h2>
                        <p>
                            -Pressure sores:  <span class="MusculoskeletalPressuresores tagReplace">MusculoskeletalPressuresores  </span>
                            <br />
                            -Degree of the sores: <span class="MusculoskeletalDegreeSores tagReplace">MusculoskeletalDegreeSores  </span>
                            <br />
                            <img class="img img-responsive" src="img/Musculoskeletal.JPG" />
                            <br />
                            - Joint stiffness or deformities <span class="MusculoskeletalJointDeformities tagReplace">MusculoskeletalJointDeformities  </span>
                            <br />
                            <br />

                        </p>
                        <hr />
                        <h2 class="text-left left marginedLeft">5- Heart Condition:  </h2>
                        <p>
                            -Free :  <span class="HeartProb tagReplace">HeartProb  </span>
                            <br />
                            -Valvular conditions: <span class="HeartValvular tagReplace">HeartValvular  </span>
                            <br />
                            - Ischemic Heart Disease: <span class="HeartIschemic tagReplace">HeartIschemic  </span>
                            <br />
                            - Arrhythmias: <span class="HeartArrhythmias tagReplace">HeartArrhythmias  </span>
                            <br />
                            -  Other Problems: <span class="HeartOtherProblems tagReplace">HeartOtherProblems  </span>
                            <br />

                        </p>
                        <br />
                        <hr />
                        <h2 class="text-left left marginedLeft">6- Renal problems:   </h2>
                        <p>
                            -Free :  <span class="RenalFree tagReplace">RenalFree  </span>
                            <br />
                            -Urolithiasis:  <span class="RenalUrolithiasis tagReplace">RenalUrolithiasis  </span>
                            <br />
                            - Impairment: <span class="RenalImpairment tagReplace">RenalImpairment  </span>Dialysis: <span class="RenalDialysis tagReplace">RenalDialysis  </span>
                            <br />
                            - Prostatic enlargement: <span class="RenalProstatic tagReplace">RenalProstatic  </span>
                            <br />
                            -  Any other problems: <span class="RenalOtherProblems tagReplace">RenalOtherProblems  </span>
                            <br />

                        </p>
                        <br />
                        <hr />
                        <h2 class="text-left left marginedLeft">7- Nutritional State:   </h2>
                        <p>
                            -State :  <span class="NutritionalState tagReplace">NutritionalState  </span>
                            <br />
                            - Obese: <span class="NutritionalObese tagReplace">NutritionalObese  </span>Morbidly Obese: <span class="NutritionalMorbidlyObese tagReplace">NutritionalMorbidlyObese  </span>
                            <br />
                            - Any other nutritional problems:  <span class="NutritionalOtherProblems tagReplace">NutritionalOtherProblems  </span>
                            <br />

                        </p>
                        <br />
                        <hr />
                        <h2 class="text-left left marginedLeft">8- Feeding    </h2>
                        <p>
                            -type :  <span class="FeedingType tagReplace">FeedingType  </span>
                            <br />

                        </p>
                        <br />
                        <hr />
                        <h2 class="text-left left marginedLeft">9- Dermatological Problems:    </h2>
                        <p>
                            -Free :  <span class="DermatologicalFree tagReplace">DermatologicalFree  </span>
                            -Present :  <span class="DermatologicalProblems tagReplace">DermatologicalProblems  </span>
                            <br />

                        </p>
                        <br />
                        <hr />
                        <h2 class="text-left left marginedLeft">10- Medications:     </h2>
                        <div>
                            <br />
                            <div class="MedicationsList textyContent marginedLeft tagReplace">1-................................ #.................. 
                                <br />
                                2-................................ #..................
                                <br />
                                3-................................ #.................. 
                                <br />
                            </div>

                        </div>
                        <br />
                        <hr />
                        <h2 class="text-left left marginedLeft">11- Recommendations:      </h2>
                        <div>
                            <div class="RecommendationsList textyContent marginedLeft tagReplace">1-................................................ 
                                <br />
                                2-.................................................
                                <br />
                                3-................................................. 
                                <br />
                            </div>

                        </div>
                        <br />
                        <p>
                            Date of evaluation  <span class="dateEvaluation tagReplace">day / month / year  </span>
                            <br />

                        </p>
                        <h2 class="left marginedLeft">Signature </h2>
                        <br />
                        <br />
                        <br />
                        <h2 class="left marginedLeft">Amoun Boutros MD., FRCS </h2>
                        <h2 class="left marginedLeft">Consultant of General Surgery </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="text-right">
        <button onclick="javascript:printDiv('reportCont');" class="btn btn-default btn-outline" type="button"><span><i class="fa fa-print"></i>Print</span> </button>
    </div>
    <div class="modal  fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 60%;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle">Hospice Evaluation on Admission</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;  </span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card card-box">

                                <div class="card-body" id="bar-parent">
                                    <div id="form_sample_1" class="form-horizontal">
                                        <div class="form-body">
                                            <header>
                                                <h3>Basic Information</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    date admission
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <div class="input-group date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="dateAdmission_input2" data-link-format="yyyy-mm-dd">
                                                        <input class="form-control input-height" id="dateAdmission" name="dateAdmission" size="16" placeholder="date admission" type="text" value="">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <input type="hidden" id="dateAdmission_input2" value="" />
                                                </div>
                                            </div>
                                            <%--input type date--%>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    patient name
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="fullName" id="fullName" data-required="1" placeholder="full name" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <%--input type text--%>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    age
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="number" name="age" id="age" data-required="1" placeholder="age" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <%--input type number--%>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    main problems 
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea name="mainProb" class="summernote" id="mainProb" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                            <%--editable text area--%>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    other problems 
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea name="basicOther" class="summernote" id="basicOther" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Vital Signs</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    pulse
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="vitalPulse" id="vitalPulse" data-required="1" placeholder="pulse" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    BP
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="vitalBP" id="vitalBP" data-required="1" placeholder="BP" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    temprature
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="vitaltemprature" id="vitaltemprature" data-required="1" placeholder="temprature" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    RR
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="vitalRR" id="vitalRR" data-required="1" placeholder="RR" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    O2 saturation
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="vitalO2Saturation" id="vitalO2Saturation" data-required="1" placeholder="O2 saturation" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Neurological state</h3>
                                            </header>

                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    glasgow
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="NeurologicalGlasgow" id="NeurologicalGlasgow" data-required="1" placeholder="Glasgow" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Eye
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="NeurologicalEye" id="NeurologicalEye" data-required="1" placeholder="Eye" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Verbal
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="NeurologicalVerbal" id="NeurologicalVerbal" data-required="1" placeholder="Verbal" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Movement
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="NeurologicalMovement" id="NeurologicalMovement" data-required="1" placeholder="Movement" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Any residual deficit
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="NeurologicalResidualDeficit" id="NeurologicalResidualDeficit" data-required="1" placeholder="Any residual deficit" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    CT or MRI of Brain summary 
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="NeurologicalMRI" id="NeurologicalMRI" class="form-control-textarea" placeholder="CT or MRI of Brain summary" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <%--text area normal--%>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Chest Condition</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Air entry
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="chestAirEntry" id="chestAirEntry" data-required="1" placeholder="Air entry" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Crepitations
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="chestCrepitations" id="chestCrepitations" data-required="1" placeholder="Crepitations" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Wheezes
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="chestWheezes" id="chestWheezes" data-required="1" placeholder="Wheezes" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    X ray chest
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="chestXRay" id="chestXRay" data-required="1" placeholder="X ray chest" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Oxygen saturation on room air
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="chestSatO2" id="chestSatO2" data-required="1" placeholder="Oxygen saturation on room air" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    %. Need for supplemental O2
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="chestSupO2" id="chestSupO2">
                                                        <option value="yes">yes</option>
                                                        <option value="no">no</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <%--select dropdown list--%>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Musculoskeletal Condition</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Pressure sores
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="MusculoskeletalPressuresores" id="MusculoskeletalPressuresores">
                                                        <option value="free">free</option>
                                                        <option value="present">present</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Degree of the sores
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="MusculoskeletalDegreeSores" id="MusculoskeletalDegreeSores">
                                                        <option value="None">None</option>
                                                        <option value="Superficial">Superficial</option>
                                                        <option value="Partial thickness">Partial thickness</option>
                                                        <option value="Full thickness">Full thickness</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Joint stiffness or deformities
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="MusculoskeletalJointDeformities" id="MusculoskeletalJointDeformities">
                                                        <option value="free no deformities">free no deformities</option>
                                                        <option value="Present">Present </option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Heart Condition</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    free
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="HeartProb" id="HeartProb">
                                                        <option value="free">yes</option>
                                                        <option value="not free">no</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Valvular conditions
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="HeartValvular" id="HeartValvular" data-required="1" placeholder="Valvular conditions" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Ischemic Heart Disease
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="HeartIschemic" id="HeartIschemic" data-required="1" placeholder=" Ischemic Heart Disease" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Arrhythmias
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="HeartArrhythmias" id="HeartArrhythmias" data-required="1" placeholder="Arrhythmias" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Other Problems 
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="HeartOtherProblems" id="HeartOtherProblems" class="form-control-textarea" placeholder="Other Problems" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Renal problems</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Free
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="RenalFree" id="RenalFree">
                                                        <option value="free">yes</option>
                                                        <option value="not free">no</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Urolithiasis
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="RenalUrolithiasis" id="RenalUrolithiasis" data-required="1" placeholder="Urolithiasis" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Impairment
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="RenalImpairment" id="RenalImpairment">
                                                        <option value="yes">yes</option>
                                                        <option value="no">no</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Dialysis
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="RenalDialysis" id="RenalDialysis">
                                                        <option value="yes">yes</option>
                                                        <option value="no">no</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Prostatic enlargement
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="RenalProstatic" id="RenalProstatic">
                                                        <option value="yes">yes</option>
                                                        <option value="no">no</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    other problems 
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="RenalOtherProblems" id="RenalOtherProblems" class="form-control-textarea" placeholder="other problems" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Nutritional State</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    State
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="NutritionalState" id="NutritionalState">
                                                        <option value="Normal">Normal</option>
                                                        <option value="Undernourished">Undernourished</option>
                                                        <option value="Cachectic">Cachectic</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Obese BMI
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="NutritionalObese" id="NutritionalObese" data-required="1" placeholder="Obese BMI" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Morbidly Obese BMI
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="NutritionalMorbidlyObese" id="NutritionalMorbidlyObese" data-required="1" placeholder="Morbidly Obese BMI" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    other problems 
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="NutritionalOtherProblems" id="NutritionalOtherProblems" class="form-control-textarea" placeholder="other problems" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Feeding</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Type
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="FeedingType" id="FeedingType">
                                                        <option value="Normal oral feeding">Normal oral feeding</option>
                                                        <option value=" Nasogastric tube feeding">Nasogastric tube feeding</option>
                                                        <option value="Gastrostomy">Gastrostomy</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Dermatological Problems</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Free
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <select class="form-control input-height" name="DermatologicalFree" id="DermatologicalFree">
                                                        <option value="Free">yes</option>
                                                        <option value="not free">no</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Problems 
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="DermatologicalProblems" id="DermatologicalProblems" class="form-control-textarea" placeholder="leave empty if none" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Medications</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    list and duration for medicins 
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea name="MedicationsList" class="summernote" id="MedicationsList" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <header>
                                                <h3>Recommendations</h3>
                                            </header>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    Recommendations 
                                                </label>
                                                <div class="col-md-9">
                                                    <textarea name="RecommendationsList" class="summernote" id="RecommendationsList" cols="30" rows="10"></textarea>
                                                </div>
                                            </div>
                                            <br />
                                            <hr />
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    date Evaluation
                                                    <span class="required">*   </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <div class="input-group date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="dateEvaluation_input2" data-link-format="yyyy-mm-dd">
                                                        <input class="form-control input-height" id="dateEvaluation" name="dateEvaluation" size="16" placeholder="date Evaluation" type="text" value="">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <input type="hidden" id="dateEvaluation_input2" value="" />
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">cancel</button>
                    <button type="button" id="fillFormBtn" class="btn btn-primary">save</button>
                </div>
            </div>
        </div>
    </div>
    <!-- #End# Center In Page -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script src="assets/summernote/summernote.js"></script>
    <script>
        $(document).ready(function () {
            $('.summernote').summernote();
            //fillFormData();
        });

        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              '<html><head><title></title><link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /><link href="Styles/deathInvoice.css" rel="stylesheet" /> </head><body>' +
              '' +
              divElements + '<script src="assets/jquery.min.js"><\/script><script src="assets/bootstrap/js/bootstrap.min.js"><\/script></body>';

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;


        }
        $("#fillFormBtn").click(function () {
            fillFormData();
            closeModal();
        });
        function fillFormData() {
            var containery = $("#form_sample_1");
            var countInput = $(containery).find("input").length;
            var countTextAreaAll = $(containery).find("textarea").length;
            var countTextAreaSmart = $(containery).find("textarea.summernote").length;
            var countTextAreaNot = parseInt(parseInt(countTextAreaAll) - parseInt(countTextAreaSmart));
            var countDDL = $(containery).find("select").length;

            for (var y = 0; y < countInput; y++) {
                var idSelect = $(containery).find("select").eq(y).attr("id");
                $("." + idSelect).text($("#" + idSelect).val());
            }

            for (var z = 0; z < countDDL; z++) {
                var idInput = $(containery).find("input").eq(z).attr("id");
                $("." + idInput).text($("#" + idInput).val());
            }
            for (var x = 0; x < countTextAreaNot; x++) {
                var idInputNot = $(containery).find("textarea").eq(x).attr("id");
                $("." + idInputNot).text($("#" + idInputNot).val());

            }

            for (var i = 0; i < countTextAreaSmart; i++) {
                var idInputSmart = $(containery).find("textarea.summernote").eq(i).attr("id");
                var smartCont = $("#" + idInputSmart).next(".note-editor");
                var bodyHere = $(smartCont).find(".note-editable.panel-body").html();
                $("." + idInputSmart).html(bodyHere);

            }
            $(".tagReplace").removeClass("tagReplace");
        }
        function closeModal() {
            $("#exampleModalLong").removeClass("show");
            $(".modal-backdrop").removeClass("show");

        }
    </script>
</asp:Content>
