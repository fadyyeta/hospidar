﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="DeathInvoice.aspx.cs" Inherits="hospidar.DeathInvoice" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpTitle" runat="server">
    Death Report
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpHead" runat="server">
    <!-- Latest compiled and minified CSS -->
    <%--<link
        rel="stylesheet"
        href="https://cdn.rtlcss.com/bootstrap/v4.2.1/css/bootstrap.min.css"
        integrity="sha384-vus3nQHTD+5mpDiZ4rkEPlnkcyTP+49BhJ4wJeJunw06ZAp+wzzeBPUXr42fi8If"
        crossorigin="anonymous">--%>

    <!-- Latest compiled and minified JavaScript -->
    <%--<script src="https://cdn.rtlcss.com/bootstrap/v4.2.1/js/bootstrap.min.js" integrity="sha384-a9xOd0rz8w0J8zqj1qJic7GPFfyMfoiuDjC9rqXlVOcGO/dmRqzMn34gZYDTel8k" crossorigin="anonymous"></script>--%>
    <style>
        #exampleModalLong {
            /*display: block;
            margin: auto;
            max-width: initial;
            overflow-y: auto !important;*/
        }
    </style>
    <link href="Styles/deathInvoice.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cpContent" runat="server">
    <!-- #Start# Center In Page -->
    <div class="row" style="margin-top: 30px;" dir="rtl">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="card-head">
                    <header>تبليغ وفاة </header>
                </div>
                <div class="card-body ">
                    <div class="col-lg-12 p-t-10">
                        <h4><strong>إضافة بيانات المتوفي</strong></h4>
                        <p>إضغط علي الزر بالأسفل لإضافة بيانات المتوفي.</p>
                        <button type="button"
                            class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect btn-circle btn-primary"
                            data-toggle="modal" data-target="#exampleModalLong">
                            إضافة</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- #Start# Center In Page -->
    <div class="row" style="margin-top: 30px;" dir="rtl">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="card-head">
                    <header>شكل التقرير </header>
                </div>
                <div class="card-body " id="reportCont">
                    <div class="col-lg-12 p-t-10 reportCont">
                        <img class="img-responsive right marginedRight" style="height: 200px;" src="logo/arabicLogo.png" />
                        <h2 class="text-center"><strong>تبليغ عن حالة وفاة</strong></h2>
                        <br />
                        <br />
                        <p>تشهد إدارة الدار ان السيد /  <span class="fullName tagReplace">الإسم بالكامل</span>  و البالغ من العمر  <span class="age tagReplace">العمر</span>  عاماً و يحمل بطاقة رقم قومي  <span class="IDNum tagReplace">الرقم القومي</span>  و تاريخ ميلاده  <span class="DOB tagReplace">يوم/شهر/سنة</span>  كان مقيماً فى الدار إعتباراً من  <span class="residentSince tagReplace">يوم/شهر/سنة</span>   و كان يعاني من  <span class="illness tagReplace">الأمراض التي كان يعاني منها</span> و أصيب ب  <span class="causeDeath tagReplace">الأسباب التي أدت الي تدهور حالته</span>  حيث أدي ذلك إلي حدوث تدهور فى وظائف  <span class="whatHappen tagReplace">الأسباب التي أدت الي الوفاة</span> مما سبب هبوط في الدورة الدمويه و القلب مما أدي إلي حدوث الوفاة الساعة  <span class="houry tagReplace">الساعة</span>  من  <span class="dayy tagReplace">اليوم</span>  الموافق  <span class="timeDeath tagReplace">يوم/شهر/سنة</span> </p>
                        <br />
                        <br />
                        <h2 class="left marginedLeft">المشرف الطبي</h2>
                        <br />
                        <br />
                        <br />
                        <h2 class="left marginedLeft">دكتور آمون لويس وهبه </h2>
                        <h2 class="left marginedLeft">استشاري الجراحة العامة </h2>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="text-right">
        <button onclick="javascript:printDiv('reportCont');" class="btn btn-default btn-outline" type="button"><span><i class="fa fa-print"></i>Print</span> </button>
    </div>
    <div class="modal  fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" style="max-width: 60%;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLongTitle">بيانات المتوفي</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12 col-sm-12">
                            <div class="card card-box">

                                <div class="card-body" id="bar-parent">
                                    <div id="form_sample_1" class="form-horizontal">
                                        <div class="form-body">
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    الإسم بالكامل
                                                    <span class="required">* </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="text" name="fullName" id="fullName" data-required="1" placeholder="أدخل الإسم رباعي" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    السن
                                                    <span class="required">* </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="number" name="age" id="age" data-required="1" placeholder="السن" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    الرقم القومي
                                                    <span class="required">* </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <input type="number" name="IDNum" id="IDNum" data-required="1" placeholder="الرقم القومى" class="form-control input-height" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    تاريخ الميلاد
                                                    <span class="required">* </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <div class="input-group date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                                        <input class="form-control input-height" id="DOB" name="DOB" size="16" placeholder="تاريخ الميلاد" type="text" value="">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <input type="hidden" id="dtp_input2" value="" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    مقيم فى الدار منذ
                                                    <span class="required">* </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <div class="input-group date form_date " data-date="" data-date-format="dd MM yyyy" data-link-field="dtp_input2" data-link-format="yyyy-mm-dd">
                                                        <input class="form-control input-height" size="16" id="residentSince" name="residentSince" placeholder="مقيم فى الدار منذ" type="text" value="">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <input type="hidden" id="dtp_input2" value="" />
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    يعانى من 
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="illness" class="form-control-textarea" placeholder="يعانى من" id="illness" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    أصيب ب 
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="causeDeath" id="causeDeath" class="form-control-textarea" placeholder="أصيب ب" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    أدي ذلك الي 
                                                </label>
                                                <div class="col-md-5">
                                                    <textarea name="whatHappen" id="whatHappen" class="form-control-textarea" placeholder="أدي ذلك الي" rows="5"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label class="control-label col-md-3">
                                                    توقيت الوفاة
                                                    <span class="required">* </span>
                                                </label>
                                                <div class="col-md-5">
                                                    <div class="input-group date form_datetime " data-link-field="dtp_input2">
                                                        <input class="form-control input-height" size="16" id="timeDeath" name="timeDeath" placeholder="توقيت الوفاة" type="text" value="">
                                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                                    </div>
                                                    <input type="hidden" id="dtp_input2" value="" />
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">إلغاء</button>
                    <button type="button" id="fillFormBtn" class="btn btn-primary">حفظ</button>
                </div>
            </div>
        </div>
    </div>
    <!-- #End# Center In Page -->
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="cpScript" runat="server">
    <script>
        function printDiv(divID) {
            //Get the HTML of div
            var divElements = document.getElementById(divID).innerHTML;
            //Get the HTML of whole page
            var oldPage = document.body.innerHTML;

            //Reset the page's HTML with div's HTML only
            document.body.innerHTML =
              '<html><head><title></title><link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" /><link href="Styles/deathInvoice.css" rel="stylesheet" /> </head><body>' +
              '' +
              divElements + '<script src="assets/jquery.min.js"><\/script><script src="assets/bootstrap/js/bootstrap.min.js"><\/script></body>';

            //Print Page
            window.print();

            //Restore orignal HTML
            document.body.innerHTML = oldPage;


        }
        $("#fillFormBtn").click(function () {
            fillFormData();
            closeModal();
        });
        function fillFormData() {
            $(".fullName").text($("#fullName").val());
            $(".age").text($("#age").val().toArDigits());
            $(".IDNum").text($("#IDNum").val().toArDigits());
            var myDOB = $("#DOB").val();
            $(".DOB").text(toArDate(myDOB).toArDigits());
            var myRDate = $("#residentSince").val();
            $(".residentSince").text(toArDate(myRDate).toArDigits());
            $(".illness").text($("#illness").val());
            $(".causeDeath").text($("#causeDeath").val());
            $(".whatHappen").text($("#whatHappen").val());
            timeOfDeath();
            $(".tagReplace").removeClass("tagReplace");
        }
        function closeModal() {
            $("#exampleModalLong").removeClass("show");
            $(".modal-backdrop").removeClass("show");

        }
        String.prototype.toArDigits = function () {
            var id = ['۰', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩'];
            return this.replace(/[0-9]/g, function (w) {
                return id[+w]
            });
        }
        function toArDate(dateVar) {
            var date = new Date(dateVar);
            var months = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو",
              "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"
            ];
            var days = ["اﻷحد", "اﻷثنين", "الثلاثاء", "اﻷربعاء", "الخميس", "الجمعة", "السبت"];
            var delDateString = days[date.getDay()] + ', ' + date.getDate() + ' ' + months[date.getMonth()] + ', ' + date.getFullYear();
            return delDateString;

        }
        function timeOfDeath() {
            var fullTimeDeath = $("#timeDeath").val();
            var date = new Date(fullTimeDeath);
            var days = ["اﻷحد", "اﻷثنين", "الثلاثاء", "اﻷربعاء", "الخميس", "الجمعة", "السبت"];
            var months = ["يناير", "فبراير", "مارس", "إبريل", "مايو", "يونيو",
                          "يوليو", "أغسطس", "سبتمبر", "أكتوبر", "نوفمبر", "ديسمبر"
            ];
            var thatDay = days[date.getDay()];
            var delDateString = date.getDate() + ' \\ ' + months[date.getMonth()] + ' \\ ' + date.getFullYear();
            var deathHour = formatAMPM(date);
            $(".houry").text(deathHour.toArDigits());
            $(".dayy").text(thatDay);
            $(".timeDeath").text(delDateString.toArDigits());
        }
        function formatAMPM(date) {
            var hours = date.getHours();
            var minutes = date.getMinutes();
            var ampm = hours >= 12 ? 'مساءً' : 'صباحاً';
            hours = hours % 12;
            hours = hours ? hours : 12; // the hour '0' should be '12'
            minutes = minutes < 10 ? '0' + minutes : minutes;
            var strTime = hours + ':' + minutes + ' ' + ampm;
            return strTime;
        }
    </script>
</asp:Content>
